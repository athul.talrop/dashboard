import Image from 'next/image';
import React from 'react';
import downicon from "@/assets/images/chevron-up.svg"
import edit from "@/assets/images/edit-05.svg"
import refresh from "@/assets/images/refresh-cw-03.svg"
import arrowup from "@/assets/images/arrow-up.svg"
import arrowdn from "@/assets/images/Icondown.svg"

function cards() {
  return (
    <div className='w-full'>
        <div className='flex justify-between py-8 border-t-2 mx-5'>
            <div className='flex gap-2 border-2 border-gray-200 rounded-md px-3 py-1 w-[250px] justify-between items-center'>
               <p>January 23, 2024</p>
               <Image src={downicon} height={10} width={20} alt='arrowupimg' />
            </div>
            <div className='flex gap-5'>
                <div className='flex justify-between gap-2 items-center border-2 border-gray-200 rounded-md px-[12px] py-[8px]'>
                    <Image src={refresh} height={10} width={20} alt='arrowupimg' />
                    <p>Refresh</p>
                </div>
                <div className='flex justify-between gap-2 items-center border-2 border-gray-200 rounded-md px-[12px] py-[8px]'>
                    <Image src={edit} height={20} width={20} alt='arrowupimg' />
                    <p>Edit summary</p>
                </div>
            </div>
        </div>
        <div className='flex items-center px-[32px] py-[24px] gap-10'>
            <div className='w-[260px] h-[160px] bg-red-500 p-[24px] rounded-2xl flex flex-col justify-between text-white ' style={{ background: 'linear-gradient(91deg, #4428EB, #B2A6F7)'}}>
                <div className='flex justify-between items-start'>
                    <h2 className='text-3xl'>12k</h2>
                    <div className='flex bg-white px-[5px] py-[2px] rounded-full items-center'>
                        <Image src={arrowup} height={20} width={20} alt='arrowupimg' />
                        <p className='text-blue-900'>100</p>
                    </div>
                </div>
                <h2>Subscribers</h2>
            </div>
            <div className='w-[260px] h-[160px] bg-red-500 p-[24px] rounded-2xl flex flex-col justify-between text-white bg-gradient-to-r from-blue-500 via-blue-500 to-blue-300'>
                <div className='flex justify-between items-start'>
                    <h2 className='text-3xl'>50k</h2>
                    <div className='flex bg-white px-[6px] py-[2px] rounded-full items-center gap-1'>
                        <Image src={arrowdn} height={20} width={15} alt='arrowupimg' />
                        <p className='text-blue-900'>5</p>
                    </div>
                </div>
                <h2>Orders</h2>
            </div>
            <div className='w-[260px] h-[160px] bg-red-500 p-[24px] rounded-2xl flex flex-col justify-between text-white bg-gradient-to-r from-orange-600 via-orange-500 to-orange-300' >
                <div className='flex justify-between items-start'>
                    <h2 className='text-3xl'>108k</h2>
                    <div className='flex bg-white px-[6px] py-[2px] rounded-full items-center gap-1'>
                        <Image src={arrowdn} height={20} width={15} alt='arrowupimg' />
                        <p className='text-blue-900'>50K</p>
                    </div>
                </div>
                <h2>Revenue</h2>
            </div>
            <div className='w-[260px] h-[160px] bg-red-500 p-[24px] rounded-2xl flex flex-col justify-between text-white bg-gradient-to-r from-pink-500 via-pink-400 to-pink-300'>
                <div className='flex justify-between items-start'>
                    <h2 className='text-3xl'>10</h2>
                    <div className='flex bg-white px-[6px] py-[2px] rounded-full items-center'>
                        <Image src={arrowup} width={20} height={10} alt='arrowimg' />
                        <p className='text-blue-900'>10K</p>
                    </div>
                </div>
                <h2>Inquiries</h2>
            </div>
        </div>
    </div>
  )
}

export default cards;