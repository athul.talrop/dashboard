import React from "react";
import mainimage1 from "@/assets/images/bell-03.svg";
import mainimage2 from "@/assets/images/filter-funnel-01.svg";
import avatarimage from "@/assets/images/Avatar.png";
import Image from "next/image";
import threedot from "@/assets/images/dots-vertical.svg";
import refresh from "@/assets/images/refresh-cw-03.svg";

export default function header() {
  return (
    <div className="w-full h-[130px]">
      <div className="justify-between items-center px-5 flex py-5">
        <div className="flex items-center">
          <div className="ml-5">
            <h1 className="text-3xl font-bold">Overview</h1>
            <div className="pt-2 flex items-center space-x-2">
              <Image
                src={refresh}
                alt="analytics"
                width={20}
                height={20}
                className=""
              />
              <p className="text-gray-500">Last Update 1 hour ago</p>
            </div>
          </div>
        </div>
        <div className="items-center">
          <div className="ml-5">
            <div className="flex items-center mt-3">
              <div className="flex">
                <Image
                  src={mainimage2}
                  alt="Main Image 2"
                  width={25}
                  height={30}
                  className="rounded-lg mr-5"
                />
                <div className="flex-shrink-0">
                  <Image
                    src={mainimage1}
                    alt="Main Image 1"
                    width={25}
                    height={30}
                    className="rounded-lg mr-5"
                  />
                </div>
              </div>
              <Image
                src={avatarimage}
                alt="Avatar"
                width={40}
                height={40}
                className="rounded-full "
              />
              <span className="ml-2 mr-3">John Doe</span>
              <div className="flex-shrink-0">
                <Image
                  src={threedot}
                  alt="Main Image 1"
                  width={20}
                  height={20}
                  className="rounded-lg"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
