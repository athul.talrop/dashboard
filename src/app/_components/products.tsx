import Image from "next/image";
import React from "react";
import searchicon from "@/assets/images/search-lg.svg";
import left from "@/assets/images/chevron-left.svg";
import right from "@/assets/images/chevron-right.svg";
import threedot from "@/assets/images/dots-vertical.svg";
import uikit from "@/assets/images/Component 392.png";
import component396 from "@/assets/images/Component 396.png";
import component397 from "@/assets/images/Component 397.png";
import component398 from "@/assets/images/Component 398.png";
import component393 from "@/assets/images/Component 393.png";
import component394 from "@/assets/images/Component 394.png";

export default function products() {
  const items = [
    {
      id: 1,
      name: "Product Design Handbook",
      dateAdded: "Dec 19, 2023",
      SKU: "PDH88801",
      price: "30.00",
      purchases: "30.00",
      image: component396,
    },
    {
      id: 2,
      name: "Website UIKit",
      dateAdded: "Dec 19, 2023",
      SKU: "PDH88801",
      price: "80.00",
      purchases: "30.00",
      image: uikit,
    },
    {
      id: 3,
      name: "Icon UIKit",
      dateAdded: "Dec 19, 2023",
      SKU: "PDH88801",
      price: "30.00",
      purchases: "30.00",
      image: component397,
    },
    {
      id: 4,
      name: "Wireframing Kit ",
      dateAdded: "Dec 19, 2023",
      SKU: "PDH88801",
      price: "8.00",
      purchases: "30.00",
      image: component398,
    },
    {
      id: 5,
      name: "Landing Page Template",
      dateAdded: "Dec 19, 2023",
      SKU: "PDH88801",
      price: "30.00",
      purchases: "30.00",
      image: component393,
    },
    {
      id: 6,
      name: "E- commerce Web  Template",
      dateAdded: "Dec 19, 2023",
      SKU: "PDH88801",
      price: "10.00",
      purchases: "30.00",
      image: component394,
    },
  ];
  return (
    <div className="w-full px-[32px] py-[24px] h-[50vh]">
      <div className="flex justify-between pb-5">
        <h2 className="text-lg font-semibold pt-2">Products</h2>
        <div className="flex gap-8">
          <div className="flex gap-2 items-center">
            <Image src={left} width={35} height={10} alt="left" />
            6 OF 20
            <Image src={right} width={35} height={10} alt="right" />
          </div>
          <div className="px-4 py-2 border border-gray-300 rounded-lg pr-8 flex items-center gap-2">
            <Image
              src={searchicon}
              alt="search"
              width={20}
              height={20}
              className=""
            />
            <input type="text" placeholder="Search" className="" />
          </div>
        </div>
      </div>
      <div className="w-full">
        <div className="px-[24px] py-[12px] flex border-b gap-16 bg-gray-50">
          <h2 className="w-[320px]">Name</h2>
          <h2 className="w-[182px]">Date added</h2>
          <h2 className="w-[182px]">SKU</h2>
          <h2 className="w-[182px]">Price</h2>
          <h2 className="w-[182px]">Purchases</h2>
          <h2 className="items-end">
            <Image src={threedot} width={25} height={10} alt="threedot" />
          </h2>
        </div>
        {items.map((item) => (
          <div className="border-b flex px-[24px] py-[12px] gap-16 items-center w-[100%] h-[80%] ">
            <p className="w-[320px] flex gap-2 items-center">
              <Image
                src={item.image}
                width={35}
                height={10}
                alt="Website UIKit"
              />
              {item.name}
            </p>
            <p className="w-[182px]">{item.dateAdded}</p>
            <p className="w-[182px]">{item.SKU}</p>
            <p className="w-[182px]">{item.price}</p>
            <p className="w-[182px]">{item.purchases}</p>
            <div className="items-end">
              <Image src={threedot} width={25} height={10} alt="threedot" />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
