import React from "react";
import Header from "./_components/header";
import Cards from "./_components/cards";
import Products from "./_components/products";

export default function page() {
  return (
    <div className="flex flex-col w-full h-[100%]">
      <Header />
      <Cards />
      <Products />
    </div>
  );
}
