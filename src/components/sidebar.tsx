import React from "react";
import Image from "next/image";
import textlogo from "@/assets/images/Textlogo.png";
import component391 from "@/assets/images/Component 391.png";
import searchicon from "@/assets/images/search-lg.svg";
import viewlive from "@/assets/images/trend-up-02.svg";
import overview from "@/assets/images/home-line.svg";
import analytics from "@/assets/images/bar-chart-square-02.svg";
import page from "@/assets/images/layers-three-01.svg";
import plus from "@/assets/images/Icon.svg";
import products from "@/assets/images/package.svg";
import tags from "@/assets/images/tag-01.svg";
import server from "@/assets/images/server-03.svg";
import member from "@/assets/images/users-01.svg";
import mail from "@/assets/images/mail-01.svg";
import { serialize } from "v8";

export default function Sidebar() {
  return (
    <div className="flex flex-col w-[20%] min-h-screen max-h-screen md:pr-8 pr-3 gap-3 border-r-[1px]">
      <div className="flex items-center space-x-2 p-5 pb-0">
        <Image
          src={component391}
          alt="logo"
          width={50}
          height={50}
          className=""
        />
        <Image src={textlogo} alt="logo" width={100} height={50} />
      </div>

      <div className="p-2 pl-5">
        <div className="flex items-center space-x-2">
          <div className="px-4 py-2 border border-gray-300 rounded-lg pr-8 flex items-center gap-2">
            <Image
              src={searchicon}
              alt="search"
              width={20}
              height={20}
              className=""
            />
            <input type="text" placeholder="Search" className="" />
          </div>
        </div>
      </div>

      <div className="flex justify-between p-2 pl-5 items-center">
        <div className="flex gap-2">
          <Image src={server} alt="pages" width={20} height={20} className="" />
          <p className="text-gray-600">View live site</p>
        </div>
        <Image src={viewlive} alt="plus" width={20} height={20} className="" />
      </div>
      <div className="pl-3 pr-3">
        <hr className="border-t border-gray-300 my-5" />
      </div>

      <div className="p-2 pl-5 flex items-center space-x-2">
        <Image
          src={overview}
          alt="overview"
          width={20}
          height={20}
          className=""
        />
        <p className="text-gray-600">Overview</p>
      </div>

      <div className="p-2 pl-5 flex items-center space-x-2">
        <Image
          src={analytics}
          alt="analytics"
          width={20}
          height={20}
          className=""
        />
        <p className="text-gray-600">Analytics</p>
      </div>
      <div className="flex justify-between p-2 pl-5 items-center">
        <div className="flex gap-2">
          <Image src={page} alt="pages" width={20} height={20} className="" />
          <p className="text-gray-600">Pages</p>
        </div>
        <Image src={plus} alt="plus" width={15} height={20} className="" />
      </div>
      <div className="p-2 pl-5 flex items-center space-x-2">
        <Image
          src={products}
          alt="analytics"
          width={20}
          height={20}
          className=""
        />
        <p className="text-gray-600">Products</p>
      </div>
      <div className="p-2 pl-5 flex items-center space-x-2">
        <Image src={tags} alt="analytics" width={20} height={20} className="" />
        <p className="text-gray-600">Tags</p>
      </div>
      <div className="flex justify-between p-2 pl-5 items-center">
        <div className="flex gap-2">
          <Image src={member} alt="pages" width={20} height={20} className="" />
          <p className="text-gray-600">Member</p>
        </div>
        <Image src={plus} alt="plus" width={15} height={20} className="" />
      </div>
      <div className="p-2 pl-5 flex items-center space-x-2">
        <Image src={mail} alt="analytics" width={20} height={20} className="" />
        <p className="text-gray-600">Inquiries</p>
      </div>
    </div>
  );
}
